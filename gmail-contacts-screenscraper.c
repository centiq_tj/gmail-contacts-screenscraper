/* convert a Gmail contacts list HTML screen-scrape into CSV
 *
 * (C) Copyright 2013 Centiq Ltd.
 * written by TJ <tj@centiq.co.uk>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>

#define MMAP_SIZE 0x100000

int
main(int argc, char **argv, char **env) {
    int i;
    char tmp;
    char *html = NULL;
    char *html_p = NULL;
    char *span_start, *span_end, *email_start, *email_end, *name_start, *name_end;
    int html_fd;
    int html_len;
    int debug = 0, ret = 0;
    struct stat html_stat;

    span_start = span_end = NULL;
    email_start = email_end = NULL;
    name_start = name_end = NULL;

    if (argc == 1) {
        fprintf(stderr, "gmail-contacts-scraper\n"
                        "(c) Copyright 2013 Centiq Ltd, written by TJ <tj@centiq.co.uk>\n"
                        "Licensed on the terms of the GNU General Public License version 3\n\n"
                        "Usage: %s [--html path/to/file.html]\n\n"
                        "file.html is the GMail HTML contact list web-page captured locally, output is to\n"
                        "stdout in comma separated values (CSV) format with header, 2 columns:\n"
                        "\"Full Name\", \"Email\"\n",
                        argv[0]);
        return 1;
    }
    for (i = 0; i < argc; i++) {
        if (strncmp("--html", argv[i], 6) == 0) {
            if (i < argc-1) {
                html = argv[i+1];
                i = i + 1;
                if (debug) fprintf(stderr, "HTML input: %s\n", html);
             } else {
                 fprintf(stderr, "%s\n", "--html requires name of HTML screen-scrape file");
                 return 2;
             }
        } else if (strncmp("--debug", argv[i], 7) == 0) {
            debug = 1;
        }

    }

    html_fd = open(html, O_RDONLY);
    if (fstat(html_fd, &html_stat) < 0) {
        perror("cannot stat HTML file");
        return 3;
    }

    if ((html_p = mmap(NULL, html_stat.st_size, PROT_READ | PROT_WRITE, MAP_PRIVATE, html_fd, 0)) > 0)  {
        printf("\"%s\",\"%s\"\n", "Full Name", "Email");
        html_len = strlen(html_p);
        if (debug) fprintf(stderr, "DEBUG: memory-mapping %ld bytes of %s (%d)\n", (unsigned long)html_stat.st_size, html, html_len);
        for (char *p = html_p; p < html_p + html_len; p++) {
            if (strncmp("<span ", p, 6) == 0)
               span_start = p;
            if (strncmp("</span>", p, 7) == 0)
               span_end = p;
            if (span_start && span_end) {
                // if (debug) fprintf(stderr, "DEBUG: found %s\n", "span");
                for (char *q = span_start; q <= span_end; q++) {
                    if (strncmp("email=", q, 6) == 0)
                       email_start = q + 7;
                    if (email_start && !email_end &&  *q == '>') {
                        email_end = q - 1;
                        name_start = q + 1;
                        if (debug) fprintf(stderr, "DEBUG: found email address%s\n", "");
                    }
                    if (name_start && strncmp("</span>", q, 7) == 0) {
                        name_end = q;
                        break;
                    }
                    if(email_start && email_end && name_start && name_end) {
                        tmp = *name_end;
                        *name_end = 0;
                        printf("\"%s\", ", name_start);
                        *name_end = tmp;

                        tmp = *email_end;
                        *email_end = 0;
                        printf("%s\n", email_start);
                        *email_end = tmp;

                        p = span_end + 7;
                        span_start = span_end = NULL;
                        email_start = email_end = NULL;
                        name_start = name_end = NULL;
                    }
                } 
            }
        }
        munmap(html_p, html_stat.st_size); 

    } else {
        fprintf(stderr, "%s\n", "Error: unable to memory-map HTML file");
    }
    close(html_fd);

    return ret;
}

