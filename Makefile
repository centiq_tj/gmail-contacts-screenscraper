TARGET = gmail-contacts-screenscraper
SRCS = $(TARGET).c
INCLUDES=
LDFLAGS=
LIBS =

CC = gcc
CFLAGS = -std=c99 -g -Wall
OBJS = $(SRCS:.c=.o)

.PHONY: clean

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(CFLAGS) $(INCLUDES) -o $(TARGET) $(OBJS) $(LDFLAGS) $(LIBS)

.c.o:
	$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $@

clean:
	$(RM) *.o *~ $(TARGET)

debug: all
	gdb --args ./$(TARGET) --html gmail.html
